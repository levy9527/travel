var gulp = require('gulp'),
	sass = require('gulp-sass'),
	//jade = require('gulp-jade'),
	browserSync = require('browser-sync').create();

var path = {
	jade: './src/jade/*.jade',
	sass: './src/sass/*.scss',
};

gulp.task('jade', function(){
	gulp.src(path.jade)
		.pipe(jade({
			pretty:true
		}))
		.pipe(gulp.dest('./build/html/'));
});

//如何把所有scss压缩成一个?
gulp.task('sass', function(){
	gulp.src(path.sass)
		.pipe(sass({
			outputStyle: 'compressed'
		}))
		.pipe(gulp.dest('./build/css/'));
});

// 静态服务器
gulp.task('server', function() {
	browserSync.init({
		port:9000,
		server: {
			baseDir: "./build/",
			directory:true,
		}
	});

	//gulp.watch([path.jade].concat(path.sass),
	//	['jade','sass']);
});

gulp.task('default',['jade','sass','server'], function(){
	//watch要concat，则参数要指定为数组,不然会出错
	gulp.watch([path.jade].concat(path.sass),
		['jade','sass']);
});