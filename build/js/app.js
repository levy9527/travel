/**
 * Created by levy on 16/3/14.
 */
//for debug
var log = function(msg){
    console.log(msg);
};

//footprint
function footprint(){
    var $footprint = $('#footprint'),
        $diary = $footprint.find('.diary');

    $diary.each(function(i,diary){
        diary.style.display = 'none';
    });

    $footprint.children('#gallery').find('a').bind('click',function(){
        var $ele = $(this),
            id = $ele.attr('href').split('#')[1];

        //$ele.parent().siblings().hide();

        $diary.each(function(i,diray){
            if(id == diray.getAttribute('id'))
                diray.style.display = 'block';
            else
                diray.style.display = 'none';
        });

    });
}

//article
function showArticle(href){
    if(href.indexOf('article') == -1) return;

    var id = href.split('#')[1];
    document.getElementById(id).style.display = 'block';
}

function highlightNav(href){
    $('nav').find('a').each(function(i,link){
       if(href.indexOf(link.getAttribute('href')) > -1){
           link.parentNode.className = "active";
       }
    });
}

//customize
function nextStep(href){
    if(href.indexOf('customize') == -1) return;

    var $div = $('#customize .first-step');

    $div.find('button').bind('click',function(){
        $div.hide().next().css('transform','translateY(0)');
    });
}
function back2Top(){
    var $btn = $('#back2top');

    $btn.click(function(){
        $('body').animate({scrollTop:0},1000);
    });

    $(window).scroll(function(){
        if($(document).scrollTop() > 200){
            $btn.css({'opacity':'1'});
        }
        else{
            $btn.css({'opacity':'0'});
        }
    });
}

$(function(){
    //我日，居然要图片加载完成才ready?
//alert('ready');
    back2Top();
    footprint();

    var href = window.location.href;

    showArticle(href);
    highlightNav(href);
    nextStep(href);

});
